#include<iostream>
using namespace std;

class Box {

	int l, b, h;

public:



	Box();
	Box(int, int, int);
	Box(const Box&);

	int getLength();
	int getBreadth();
	int getHeigth();
	long long CalculateVolume();
	friend bool operator < (Box& A, Box& B) {

		if ((A.l < B.l) || ((A.b < B.b) && (A.l == B.l)) || ((A.h < B.h) && (A.l == B.l) && (A.b == B.b)))
			return true;
		else
			return false;
	}

	friend ostream& operator << (ostream& output, Box& B) {

		output << B.l << " " << B.b << " " << B.h;
		return output;
	}
};


Box::Box() { l = 0, b = 0, h = 0; }
Box::Box(int l, int b, int h) {

	this->l = l;
	this->b = b;
	this->h = h;
}

Box::Box(const Box& c) {

	l = c.l;
	b = c.b;
	h = c.h;
}

int Box::getLength() { return l; }
int Box::getBreadth() { return b; }
int Box::getHeigth() { return h; }
long long Box::CalculateVolume() { return (long long)h * b * l; }


void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
	cout << endl << endl;
	return 0;
}