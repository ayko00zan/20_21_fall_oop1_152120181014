#include <iostream>
using namespace std;

//Sample Input
//
//4
//5
//Sample Output
//
//9
//1

void update(int* a, int* b) {


    int add = *a + *b;
    int sub = abs(*a - *b);
    *b = sub;
    *a = add;
}

int main() {

    int a, b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}
