#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

/*Sample Input

23,4,56
Sample Output

23
4
56*/

vector<int> parseInts(string str) {

    vector<string> v;
    vector<int> nums;
    stringstream ss(str);
    string temp;
    int x = 0;
    while (ss.good()) { // end of string stream ss

        ss >> x; // string to integer
        getline(ss, temp, ','); // get string from ss and assign into string temp
        v.push_back(temp); 
        nums.push_back(x);

    }


    return nums;
}

int main() {

    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}