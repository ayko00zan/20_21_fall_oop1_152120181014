#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

class Student {
public:

    int scores[50];
    int size = sizeof(scores) / sizeof(int);
    int k = 0;
    int a, b, c, d, e;

    void input() {

        cin >> a >> b >> c >> d >> e;
        scores[k++] = a;
        scores[k++] = b;
        scores[k++] = c;
        scores[k++] = d;
        scores[k++] = e;

    }

    int calculateTotalScore() {

        int sum = 0;
        int indmax = 0;
        for (int i = 0; i < size; i++) {

            sum += scores[i];
        }
        return sum;
    }
};


int main() {
    int n; // number of students
    cin >> n;
    Student* s = new Student[n]; // an array of n students

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

    // calculate kristen's score
    int kristen_score = s[0].calculateTotalScore();

    // determine how many students scored higher than kristen
    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }

    // print result
    cout << count;

    return 0;
}
