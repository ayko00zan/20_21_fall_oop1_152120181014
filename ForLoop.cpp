#include <iostream>
#include <cstdio>
using namespace std;

int main() {

    int n, x;
    string nums[] = { "", "one","two","three","four","five","six","seven",
    "eight","nine" };
    cin >> n >> x;

    for (int i = n; i <= x; i++) {

        if (i >= 1 && i <= 9)
            cout << nums[i] << endl;

        else {
            if (i % 2 == 0)
                cout << "even" << endl;
            else
                cout << "odd" << endl;
        }
    }
    return 0;
}
