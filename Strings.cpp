#include <iostream>
#include <string>
using namespace std;

/*Output Format

In the first line print two space-separated integers, representing the length of  and  respectively.
In the second line print the string produced by concatenating  and  ().
In the third line print two strings separated by a space and except that their first characters are swapped.

Sample Input

abcd
ef

Sample Output

4 2
abcdef
ebcd af*/

int main() {

	string str1;
	string str2;
	cin >> str1 >> str2;

	cout << str1.length() << " " << str2.length() << endl;
	cout << str1 + str2 << endl;
	char c = str1[0]; // a
	str1[0] = str2[0];
	str2[0] = c;

	cout << str1 << " " << str2 << endl;

	cout << endl << endl;
	return 0;
}