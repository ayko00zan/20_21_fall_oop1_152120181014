#include<iostream>
#include<fstream>
#include<sstream>
using namespace std;

void get_data(fstream&);
float Sum(float[], int);
float Product(float[], int);
float Smallest(float[], int);
float Average(float[], int);


int main() {



    bool status;
    char filename[81] = "input.txt";
    fstream data_file;

    data_file.open(filename, ios::in);

    if (data_file.fail())
        status = false;
    else
        status = true;

    if (status == false) {
        cout << "The file was not opened";
        exit(0);
    }
    cout << "The file was opened successfully" << endl;

    get_data(data_file);


    cout << endl << endl;
    return 0;
}

void get_data(fstream& file) {

    int n, i = 0, x;
    float nums[81];
    int a;
    ifstream fin("input.txt");
    char chosen_type = 'a';
    fin >> n;
    cout << "Number of terms: " << n << endl;
    cout << "The numbers: ";
    while (fin >> a) {

        cout << a << "  ";
        nums[i++] = a;

    }


    if (i != n) {

        cout << "\nInput error!";
        exit(0);
    }



    cout << "\nSum is " << Sum(nums, i) << endl;
    cout << "Product is " << Product(nums, i) << endl;
    cout << "Smallest is " << Smallest(nums, i) << endl;
    cout << "Average is " << Average(nums, i) << endl;


}

float Sum(float a[], int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) {

        sum += a[i];
    }
    return sum;
}

float Product(float a[], int size) {
    int prdct = 1;
    for (int i = 0; i < size; i++) {

        prdct *= a[i];
    }
    return prdct;
}

float Smallest(float a[], int size) {
    int min = a[0];
    for (int i = 1; i < size; i++)
        if (a[i] < min)
            min = a[i];

    return min;
}

float Average(float a[], int size) {
    int sum = 0, avg=0;
    for (int i = 0; i < size; i++)
        sum += a[i];

    avg = sum / size;
    return avg;
}