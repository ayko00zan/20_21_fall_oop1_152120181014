#include <iostream> 
#include <vector> 
using namespace std;

int main() {

	int n, q, c, r, a;
	vector<vector<int>> map;
	cout << "Input number of vectors and queries separated by space character:" << endl;
	cin >> n >> q;

	for (int i = 0; i < n; ++i) {

		cin >> c;
		vector<int> row;

		for (int j = 0; j < c; ++j) {

			int value;
			// Input elements of vector
			cin >> value;
			row.push_back(value);
		}

		map.push_back(row);
	}

	cout << "Coordinates:" << endl;
	for (int i = 0; i < q; i++) {
		cin >> r >> a;
		cout << map[r][a] << endl;
	}

	cout << endl << endl;
	return 0;
}